#include <iostream>
#include <ctime>
#define M 10000
#define N 1000

int* insertX(int n, int arr[], int x, int pos)
{
    int i;

    n++;
    for (i = n; i >= pos; i--)
        arr[i] = arr[i - 1];
    arr[pos - 1] = x;

    return arr;
}

struct T_List
{
    T_List* next;
    int age;
};

int size = 0;

void Add(T_List* head, int age)
{
    T_List* p = new T_List;
    p->age = age;

    p->next = head->next;
    head->next = p;
}

void printList(struct T_List* head)
{
    while (head != nullptr) 
    {
        std::cout << " " << head->age;
        head = head->next;
    }
    std::cout << std::endl;
}

T_List* getNode(int data)
{
    T_List* newNode = new T_List();

    newNode->age = data;
    newNode->next = NULL;
    return newNode;
}

void insertPos(T_List** current, int k, int data)
{
    if (k < 1 || k > size + 1)
        std::cout << "Invalid position!" << std::endl;
    else {
        while (k--) {
            if (k == 0) {
                T_List* temp = getNode(data);
                temp->next = *current;
                *current = temp;
            }
            else
                current = &(*current)->next;
        }
        size++;
    }
}

int main()
{
    int a[M] = { 0 };
    int i, j;
    for (i = 0; i < M; i++)
        a[i] = rand();
    for (i = 0; i < M; i++)
        std::cout << a[i] << " ";
    std::cout << std::endl;

    int x = 0, k = 0;
    for (j = 0; j < N; j++)
    {
        x = rand() * rand() / M;
        k = rand() * rand() / N;
        insertX(M, a, x, k);
    }
    for (i = 0; i < M; i++)
        std::cout << a[i] << " ";
    std::cout << std::endl;

    T_List* head = new T_List;
    head->next = nullptr;

    for (int j = 0; j < M; j++)
        Add(head, rand());
    size = M;
    std::cout << "Linked list before insertion" << std::endl;
    printList(head);

    for (int j = 0; j < N; j++)
    {
        x = rand() * rand() / M;
        k = rand() * rand() / N;
        insertPos(&head, k, x);
        std::cout << "Linked list after insertion of 12 at position 3" << std::endl;
        printList(head);
    }
    std::cout << "Running time: " << clock() << std::endl;

    return 0;
}
