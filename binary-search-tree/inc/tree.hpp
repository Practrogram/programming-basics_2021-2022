#pragma once

namespace an {
	struct Elem
	{
		int data;

		Elem* left;
		Elem* right;
		Elem* parent;
	};
	Elem* Make(int data, Elem* p);
	void Add(int data, Elem*& root);
	void Pass(Elem* v);
	Elem* Search(int data, Elem* v);
	void Position(int data, Elem* v, int k);
	void Delete(int data, Elem*& root);
	void Clear(Elem*& v);
}