#include <iostream>
#include "tree.hpp"
#include <fstream>

using namespace an;

int main()
{
	std::ifstream in("input.txt");
	Elem* root = nullptr;
	int x;
	char s;
	while (!in.eof())
	{
		in >> s;
		if (s == ' ')
			break;
		if (s == '+')
		{
			in >> x;
			Add(x, root);
		}
		if (s == '-')
		{
			in >> x;
			Delete(x, root);
		}
		if (s == '?')
		{
			in >> x;
			Position(x, root, 1);
		}
		if (s == 'E')
			break;
	}
	Pass(root);
	Clear(root);

	return 0;
}