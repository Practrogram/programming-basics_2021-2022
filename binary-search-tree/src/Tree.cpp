#include <iostream>
#include "tree.hpp"

namespace an {

	Elem* Make(int data, Elem* p)
	{
		Elem* q = new Elem;
		q->data = data;
		q->left = nullptr;
		q->right = nullptr;
		q->parent = p;
		return q;
	}

	void Add(int data, Elem*& root)
	{
		if (root == nullptr)
		{
			root = Make(data, nullptr);
			return;
		}
		Elem* v = root;
		while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
		{
			if (data < v->data)
				v = v->left;
			else
				v = v->right;
		}
		if (data == v->data)
			return;
		Elem* u = Make(data, v);
		if (data < v->data)
			v->left = u;
		else
			v->right = u;
	}

	void Pass(Elem* v)
	{
		if (v == nullptr)
			return;
		Pass(v->left);
		std::cout << v->data << std::endl;
		Pass(v->right);
	}

	Elem* Search(int data, Elem* v)
	{
		if (v == nullptr)
			return v;
		if (v->data == data)
			return v;
		if (data < v->data)
			return Search(data, v->left);
		else
			return Search(data, v->right);
	}

	void Position(int data, Elem* v, int k)
	{
		if (v == nullptr) {
			std::cout << "n";
			return;
		}
		if (v->data == data) {
			std::cout << k;
			return;
		}
		if (data < v->data)
			return Position(data, v->left, k + 1);
		else
			return Position(data, v->right, k + 1);
	}

	void Delete(int data, Elem*& root)
	{
		Elem* u = Search(data, root);
		if (u == nullptr)
			return;

		if (u->left == nullptr && u->right == nullptr && u == root)
		{
			delete root;
			root = nullptr;
			return;
		}

		if (u->left == nullptr && u->right != nullptr && u == root)
		{
			Elem* t = u->right;
			while (t->left != nullptr)
				t = t->left;
			u->data = t->data;
			u = t;
		}

		if (u->left != nullptr && u->right == nullptr && u == root)
		{
			Elem* t = u->left;
			while (t->right != nullptr)
				t = t->right;
			u->data = t->data;
			u = t;
		}

		if (u->left != nullptr && u->right != nullptr)
		{
			Elem* t = u->right;
			while (t->left != nullptr)
				t = t->left;
			u->data = t->data;
			u = t;
		}

		Elem* t;
		if (u->left == nullptr)
			t = u->right;
		else
			t = u->left;
		if (u->parent->left == u)
			u->parent->left = t;
		else
			u->parent->right = t;
		if (t != nullptr)
			t->parent = u->parent;
		delete u;
	}

	void Clear(Elem*& v)
	{
		if (v == nullptr)
			return;
		Clear(v->left);
		Clear(v->right);
		delete v;
		v = nullptr;
	}
}
