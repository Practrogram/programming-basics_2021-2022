#include <iostream>
#include "BMP.hpp"
using namespace an;

int main()
{
    try
    {
        BMP test_bmp(500, 500);
        test_bmp.Fill({ 102,204,0 });
        test_bmp.Rotate(acos(-1) / 4);
        test_bmp.Filling();
        test_bmp.Save("test.bmp");
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

	return 0;
}