#include <iostream>
#include <algorithm>
#define N 1000

void QuickSort(int a, int b, int x[1000000])
{
	if (a >= b)
		return;
	int m = rand() * rand() % (b - a + 1) + a;
	int k = x[m];
	int l = a - 1;
	int r = b + 1;
	while (1)
	{
		while (x[l] < k)
			l++;
		while (x[r] > k)
			r--;
		if (l >= r)
			break;
		std::swap(x[l], x[r]);
	}
	r = l;
	l--;
	QuickSort(a, l, x);
	QuickSort(r, b, x);
}

void BUBBLESORT(int mas[N], int n)
{
	for (int i = 1; i < n; i++)
	{
		if (mas[i] >= mas[i - 1])
			continue;
		int j = i - 1;
		while (j >= 0 && mas[j] > mas[j + 1])
		{
			std::swap(mas[j], mas[j + 1]);
			j--;
		}
	}
}


int main()
{
	int a = 1;
	int b = 10;

	srand(time(0));
	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand();

	//QuickSort(a, b, mas);
	//BUBBLESORT(mas, N);

	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;

	return 0;
}