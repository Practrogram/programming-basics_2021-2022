#include <iostream>
#include "BMP.hpp"
using namespace an;

int main()
{
    try
    {
        BMP test_bmp;
        test_bmp.Open("flag.bmp");
        test_bmp.Filter();
        test_bmp.Save("test.bmp");
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

	return 0;
}