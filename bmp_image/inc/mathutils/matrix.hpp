#pragma once
#include <iostream>

namespace an
{
	// MY_DEBUG 
	//#define MY_DEBUG 

	template<typename T, int N, int M>
	struct MasWrapper
	{
		T mas[N][M];
	};

	template<typename T, int N, int M>
	class Matrix
	{
	public:
		Matrix()
		{
#ifdef MY_DEBUG
			std::cout << "Constructor" << std::endl;
#endif
			a_n = N;
			a_m = M;
			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					a_mat[i][j] = 0;
		}

		Matrix(const T mas[N][M])
		{
#ifdef MY_DEBUG
			std::cout << "Constructor" << std::endl;
#endif
			a_n = N;
			a_m = M;
			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					a_mat[i][j] = mas[i][j];
		}

		Matrix(const MasWrapper<T, N, M>& mas)
		{
#ifdef MY_DEBUG
			std::cout << "Constructor" << std::endl;
#endif
			a_n = N;
			a_m = M;
			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					a_mat[i][j] = mas.mas[i][j];
		}

		Matrix(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "Copy constructor" << std::endl;
#endif

			a_n = mat.a_n;
			a_m = mat.a_m;

			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					a_mat[i][j] = mat.a_mat[i][j];
		}

		int getN() const { return a_n; }
		int getM() const { return a_m; }
		T get(int i, int j) const { return a_mat[i][j]; }
		void set(int i, int j, T data) { a_mat[i][j] = data; }

		template<typename T, int N, int M>
		Matrix<T, N, M>& operator=(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "Operator =" << std::endl;
#endif

			a_n = mat.getN();
			a_m = mat.getM();

			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					a_mat[i][j] = mat.get(i, j);

			return *this;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator+(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "operator+" << std::endl;
#endif
			Matrix<T, N, M> tmp;
			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					tmp.a_mat[i][j] = a_mat[i][j] + mat.a_mat[i][j];
			return tmp;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator-(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "operator-" << std::endl;
#endif
			Matrix<T, N, M> tmp;
			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					tmp.a_mat[i][j] = a_mat[i][j] - mat.a_mat[i][j];
			return tmp;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator*(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "operator*" << std::endl;
#endif
			Matrix<T, N, M> tmp;

			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < mat.getM(); j++)
				{
					T sum = 0;
					for (int k = 0; k < a_m; k++)
						sum += a_mat[i][k] * mat.get(k, j);
					tmp.set(i, j, sum);
				}

			return tmp;
		}

		T determinant()
		{
			T det = 0;

			if ((a_n == 2) && (a_m == 2)) {
				det = a_mat[0][0] * a_mat[1][1] - a_mat[1][0] * a_mat[0][1];
				return det;
			}
			else if ((a_n == 3) && (a_m == 3)) {
				det = a_mat[0][0] * (a_mat[1][1] * a_mat[2][2] - a_mat[1][2] * a_mat[2][1]) - a_mat[0][1] * (a_mat[1][0] * a_mat[2][2] - a_mat[1][2] * a_mat[2][0]) + a_mat[0][2] * (a_mat[1][0] * a_mat[2][1] - a_mat[1][1] * a_mat[2][0]);
				return det;
			}
			else {
				std::cout << "Operation not supported!" << std::endl;
				return -1;
			}
		}

		Matrix<T, N, M> inv()
		{
			Matrix<T, N, M> tmp;
			if ((a_n == 2 && a_m == 2) || (a_n == 3 && a_m == 3)) {
				T det = determinant();
				if (det == 0) {
					std::cout << "Zero determinant!" << std::endl;
					return tmp;
				}

				if (a_n == 2) {
					tmp.a_mat[0][0] = a_mat[1][1] / det;
					tmp.a_mat[0][1] = -a_mat[0][1] / det;
					tmp.a_mat[1][0] = -a_mat[1][0] / det;
					tmp.a_mat[1][1] = a_mat[0][0] / det;
					return tmp;
				}
				if (a_n == 3) {
					tmp.a_mat[0][0] = (a_mat[1][1] * a_mat[2][2] - a_mat[2][1] * a_mat[1][2]) / det;
					tmp.a_mat[1][0] = (a_mat[1][0] * a_mat[2][2] - a_mat[2][0] * a_mat[1][2]) / det;
					tmp.a_mat[2][0] = (a_mat[1][0] * a_mat[2][1] - a_mat[2][0] * a_mat[1][1]) / det;
					tmp.a_mat[0][1] = (a_mat[0][1] * a_mat[2][2] - a_mat[2][1] * a_mat[0][2]) / det;
					tmp.a_mat[1][1] = (a_mat[0][0] * a_mat[2][2] - a_mat[2][0] * a_mat[0][2]) / det;
					tmp.a_mat[2][1] = (a_mat[0][0] * a_mat[2][1] - a_mat[2][0] * a_mat[0][1]) / det;
					tmp.a_mat[0][2] = (a_mat[0][1] * a_mat[1][2] - a_mat[1][1] * a_mat[0][2]) / det;
					tmp.a_mat[1][2] = (a_mat[0][0] * a_mat[1][2] - a_mat[1][0] * a_mat[0][2]) / det;
					tmp.a_mat[2][2] = (a_mat[0][0] * a_mat[1][1] - a_mat[1][0] * a_mat[0][1]) / det;
					return tmp;
				}
			}
			else {
				std::cout << "Operation not supported!" << std::endl;
			}
		}

		Matrix<T, N, M> transposed() {
#ifdef MY_DEBUG
			std::cout << "transposed" << std::endl;
#endif
			Matrix<T, N, M> tmp;
			for (int i = 0; i < a_n; i++)
				for (int j = 0; j < a_m; j++)
					tmp.a_mat[i][j] = a_mat[j][i];
			return tmp;
		}


		~Matrix()
		{
#ifdef MY_DEBUG
			std::cout << "Destructor" << std::endl;
#endif
		}

		template<typename T, int N, int M>
		friend std::istream& operator>>(std::istream& os, Matrix<T, N, M>& mat);

		template<typename T, int N, int M>
		friend std::ostream& operator<<(std::ostream& os, const Matrix<T, N, M>& mat);

	private:
		int a_n, a_m;
		T a_mat[N][M];
	};

	template<typename T, int N, int M>
	std::istream& operator>>(std::istream& in, Matrix<T, N, M>& mat)
	{
		for (int i = 0; i < mat.a_n; i++)
			for (int j = 0; j < mat.a_m; j++)
				in >> mat.a_mat[i][j];
		return in;
	}

	template<typename T, int N, int M>
	std::ostream& operator<<(std::ostream& out, const Matrix<T, N, M>& mat)
	{
		out << "Matrix " << mat.a_n << "x" << mat.m_m << std::endl;
		for (int i = 0; i < mat.a_n; i++) {
			for (int j = 0; j < mat.a_m; j++)
				out << mat.a_mat[i][j] << " ";
			out << std::endl;
		}
		return out;
	}

	using Vec2i = Matrix<int, 2, 1>;
	using Vec2d = Matrix<double, 2, 1>;
	using Mat22i = Matrix<int, 2, 2>;
	using Mat22d = Matrix<double, 2, 2>;
	using Vec3i = Matrix<int, 3, 1>;
	using Vec3d = Matrix<double, 3, 1>;
	using Mat33i = Matrix<int, 3, 3>;
	using Mat33d = Matrix<double, 3, 3>;
}
