#include "BMP.hpp"

namespace an
{
    BMP::BMP()
    {
        a_width = 0;
        a_height = 0;
    }

    BMP::BMP(int width, int height)
    {
        a_width = width;
        a_height = height;
        a_pixels = new Pixel * [a_height];
        for (int i = 0; i < a_height; i++)
            a_pixels[i] = new Pixel[a_width];

        a_coordinates = new Vec2d * [a_height];
        for (int i = 0; i < a_height; i++)
            a_coordinates[i] = new Vec2d[a_width];

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_pixels[i][j] = { 0,0,0 };

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
            {
                a_coordinates[i][j].set(0, 0, j);
                a_coordinates[i][j].set(1, 0, i);
            }
    }

    BMP::BMP(const BMP& bmp)
    {
        if (a_pixels != nullptr)
        {
            for (int i = 0; i < a_height; i++)
                delete[] a_pixels[i];
            delete[] a_pixels;
        }

        a_width = bmp.a_width;
        a_height = bmp.a_height;

        a_pixels = new Pixel * [a_height];
        for (int i = 0; i < a_height; i++)
            a_pixels[i] = new Pixel[a_width];

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_pixels[i][j] = bmp.a_pixels[i][j];
    }

    BMP& BMP::operator=(const BMP& bmp)
    {
        if (a_pixels != nullptr)
        {
            for (int i = 0; i < a_height; i++)
                delete[] a_pixels[i];
            delete[] a_pixels;
        }

        a_width = bmp.a_width;
        a_height = bmp.a_height;

        a_pixels = new Pixel * [a_height];
        for (int i = 0; i < a_height; i++)
            a_pixels[i] = new Pixel[a_width];

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_pixels[i][j] = bmp.a_pixels[i][j];

        return *this;
    }

    BMP::~BMP()
    {
        for (int i = 0; i < a_height; i++)
            delete[] a_pixels[i];
        delete[] a_pixels;

        for (int i = 0; i < a_height; i++)
            delete[] a_coordinates[i];
        delete[] a_coordinates;
    }

    void BMP::Fill(Pixel pixel)
    {
        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_pixels[i][j] = pixel;
    }

    void BMP::Brightness()
    {
        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
            {
                if (a_pixels[i][j].b + 50 < 256)
                    a_pixels[i][j].b += 50;
                if (a_pixels[i][j].g + 50 < 256)
                    a_pixels[i][j].g += 50;
                if (a_pixels[i][j].r + 50 < 256)
                    a_pixels[i][j].r += 50;
            }
    }

    void BMP::Open(const std::string& filename)
    {
        std::ifstream in(filename, std::ios::binary);

        BMPHEADER bmpHeader;
        in.read(reinterpret_cast<char*>(&bmpHeader), sizeof(BMPHEADER));

        BMPINFO bmpInfo;
        in.read(reinterpret_cast<char*>(&bmpInfo), sizeof(BMPINFO));

        if (a_pixels != nullptr)
        {
            for (int i = 0; i < a_height; i++)
                delete[] a_pixels[i];
            delete[] a_pixels;
        }

        a_width = bmpInfo.Width;
        a_height = bmpInfo.Height;

        a_pixels = new Pixel * [a_height];
        for (int i = 0; i < a_height; i++)
            a_pixels[i] = new Pixel[a_width];

        for (int i = 0; i < a_height; i++)
        {
            for (int j = 0; j < a_width; j++)
                in.read(reinterpret_cast<char*>(&a_pixels[i][j]), sizeof(Pixel));

            if ((3 * a_width) % 4 != 0)
                for (int j = 0; j < 4 - (3 * a_width) % 4; j++)
                {
                    char c;
                    in.read(&c, 1);
                }
        }
    }

    void BMP::Save(const std::string& filename)
    {
        if (a_width == 0 || a_height == 0)
            throw std::exception("Zero width or height!");

        std::ofstream out(filename, std::ios::binary);

        BMPHEADER bmpHeader_new;
        bmpHeader_new.Type = 0x4D42;
        bmpHeader_new.Size = 14 + 40 + (3 * a_width * a_height);
        if (a_width % 4 != 0)
            bmpHeader_new.Size += (4 - (3 * a_width) % 4) * a_height;
        bmpHeader_new.OffBits = 54;
        bmpHeader_new.Reserved1 = 0;
        bmpHeader_new.Reserved2 = 0;

        out.write(reinterpret_cast<char*>(&bmpHeader_new), sizeof(BMPHEADER));

        BMPINFO bmpInfo_new;
        bmpInfo_new.BitCount = 24;
        bmpInfo_new.ClrImportant = 0;
        bmpInfo_new.ClrUsed = 0;
        bmpInfo_new.Compression = 0;
        bmpInfo_new.Height = a_height;
        bmpInfo_new.Planes = 1;
        bmpInfo_new.Size = 40;
        bmpInfo_new.SizeImage = bmpHeader_new.Size - 54;
        bmpInfo_new.Width = a_width;
        bmpInfo_new.XPelsPerMeter = 0;
        bmpInfo_new.YPelsPerMeter = 0;

        out.write(reinterpret_cast<char*>(&bmpInfo_new), sizeof(BMPINFO));

        for (int i = 0; i < bmpInfo_new.Height; i++)
        {
            for (int j = 0; j < bmpInfo_new.Width; j++)
                out.write(reinterpret_cast<char*>(&a_pixels[i][j]), sizeof(Pixel));

            if ((3 * bmpInfo_new.Width) % 4 != 0)
                for (int j = 0; j < 4 - (3 * bmpInfo_new.Width) % 4; j++)
                {
                    char c = 0;
                    out.write(&c, 1);
                }
        }
    }

    void BMP::Filter()
    {
        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_pixels[i][j].g = 170;
    }

	void BMP::Rotate(double angle)
	{
        Vec2d T({ {
            {(double)(a_width / 2)},
            {(double)(a_height / 2)}
        } });

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_coordinates[i][j] = a_coordinates[i][j] - T;

        Mat22d R({ {
            {cos(angle), sin(angle)},
            {-sin(angle), cos(angle)}
        } });

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_coordinates[i][j] = R * a_coordinates[i][j];

        int maxX = INT_MIN;
        int minX = INT_MAX;
        int maxY = INT_MIN;
        int minY = INT_MAX;
        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
            {
                if (maxX < a_coordinates[i][j].get(0, 0))
                    maxX = a_coordinates[i][j].get(0, 0);
                if (minX > a_coordinates[i][j].get(0, 0))
                    minX = a_coordinates[i][j].get(0, 0);
                if (maxY < a_coordinates[i][j].get(1, 0))
                    maxY = a_coordinates[i][j].get(1, 0);
                if (minY > a_coordinates[i][j].get(1, 0))
                    minY = a_coordinates[i][j].get(1, 0);
            }

        maxX++;
        minX--;
        maxY++;
        minY--;

        int width = maxX - minX;
        int height = maxY - minY;

        Vec2d Shift({ {
            {(double)(width / 2)},
            {(double)(height / 2)}
        } });

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
                a_coordinates[i][j] = a_coordinates[i][j] + Shift;

        Pixel** new_pixels = new Pixel * [height];
        for (int i = 0; i < height; i++)
            new_pixels[i] = new Pixel[width];

        Vec2d** new_coordinates = new Vec2d * [height];
        for (int i = 0; i < height; i++)
            new_coordinates[i] = new Vec2d[width];

        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                new_pixels[i][j] = { 0,0,0 };

        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
            {
                new_coordinates[i][j].set(0, 0, j);
                new_coordinates[i][j].set(0, 0, i);
            }

        for (int i = 0; i < a_height; i++)
            for (int j = 0; j < a_width; j++)
            {
                int x = (int)(a_coordinates[i][j].get(0, 0));
                int y = (int)(a_coordinates[i][j].get(1, 0));
                new_pixels[y][x] = a_pixels[i][j];
            }

        for (int i = 0; i < a_height; i++)
            delete[] a_pixels[i];
        delete[] a_pixels;

        for (int i = 0; i < a_height; i++)
            delete[] a_coordinates[i];
        delete[] a_coordinates;

        a_pixels = new_pixels;
        a_coordinates = new_coordinates;

        a_width = width;
        a_height = height;
	}
}