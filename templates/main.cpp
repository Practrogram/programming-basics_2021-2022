#include <iostream>
#include <cassert>
#include "matrix.hpp"
using an::Vec2d;
using an::Vec3d;
using an::Mat22d;
using an::Mat33d;

int main()
{
	std::cout << "=== Test 1 ===" << std::endl;
	{
		Mat22d A({ {
			 {1,2},
			 {3,4}
		} });

		Vec2d X({ {
			{1},
			{1}
		} });

		auto B = A * X;

		assert(B.get(0, 0) == 3);
		assert(B.get(1, 0) == 7);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 2 ===" << std::endl;
	{
		Mat33d A({ {
			{1,2,3},
			{4,5,6},
			{7,8,9}
		} });

		Vec3d X({ {
			{1},
			{1},
			{1}
        } });

		auto B = A * X;

		assert(B.get(0, 0) == 6);
		assert(B.get(1, 0) == 15);
		assert(B.get(2, 0) == 24);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 3 ===" << std::endl;
	{
		Mat22d A({ {
			 {1,1},
			 {1,1}
		} });

		Mat22d X({ {
			{1,5},
			{1,3}
		} });

		auto B = A + X;
		assert(B.get(0, 0) == 2);
		assert(B.get(0, 1) == 6);
		assert(B.get(1, 0) == 2);
		assert(B.get(1, 1) == 4);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 4 ===" << std::endl;
	{
		Mat33d A({ {
			{1,1,1},
			{1,1,1},
			{1,1,1}
		} });

		Mat33d X({ {
			{1,2,3},
			{1,2,3},
			{1,2,3}
        } });

		auto B = A + X;
		assert(B.get(0, 0) == 2);
		assert(B.get(0, 1) == 3);
		assert(B.get(0, 2) == 4);
		assert(B.get(1, 0) == 2);
		assert(B.get(1, 1) == 3);
		assert(B.get(1, 2) == 4);
		assert(B.get(2, 0) == 2);
		assert(B.get(2, 1) == 3);
		assert(B.get(2, 2) == 4);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 5 ===" << std::endl;
	{
		Mat22d A({ {
			{5,6},
			{7,8}
        } });

		Mat22d X({ {
			{4,3},
			{2,1}
		} });

		auto B = A - X;
		assert(B.get(0, 0) == 1);
		assert(B.get(0, 1) == 3);
		assert(B.get(1, 0) == 5);
		assert(B.get(1, 1) == 7);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 6 ===" << std::endl;
	{
		Mat33d A({ {
			{81,64,49},
			{36,25,16},
			{9,4,1}
		} });

		Mat33d X({ {
			{18,16,14},
			{12,10,8},
			{6,4,2}
        } });

		auto B = A - X;
		assert(B.get(0, 0) == 63);
		assert(B.get(0, 1) == 48);
		assert(B.get(0, 2) == 35);
		assert(B.get(1, 0) == 24);
		assert(B.get(1, 1) == 15);
		assert(B.get(1, 2) == 8);
		assert(B.get(2, 0) == 3);
		assert(B.get(2, 1) == 0);
		assert(B.get(2, 2) == -1);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 7 ===" << std::endl;
	{
		Mat22d A({ {
			{2,4},
			{7,9}
        } });

		auto B = A.transposed();
		assert(B.get(0, 0) == 2);
		assert(B.get(0, 1) == 7);
		assert(B.get(1, 0) == 4);
		assert(B.get(1, 1) == 9);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 8 ===" << std::endl;
	{
		Mat33d A({ {
			{1,2,3},
			{5,7,11},
			{13,17,19}
        } });

		auto B = A.transposed();
		assert(B.get(0, 0) == 1);
		assert(B.get(0, 1) == 5);
		assert(B.get(0, 2) == 13);
		assert(B.get(1, 0) == 2);
		assert(B.get(1, 1) == 7);
		assert(B.get(1, 2) == 17);
		assert(B.get(2, 0) == 3);
		assert(B.get(2, 1) == 11);
		assert(B.get(2, 2) == 19);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 9 ===" << std::endl;
	{
		Mat22d A({ {
			{9,5},
			{16,9}
        } });

		auto det = A.determinant();
		assert(det == 1);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 10 ===" << std::endl;
	{
		Mat33d A({ {
			{6,7,7},
			{1,1,1},
			{2,2,1}
        } });

		auto det = A.determinant();
		assert(det == 1);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 11 ===" << std::endl;
	{
		Mat22d A({ {
			{5,2},
			{17,7}
        } });

		auto B = A.inv();
		assert(B.get(0, 0) == 7);
		assert(B.get(0, 1) == -2);
		assert(B.get(1, 0) == -17);
		assert(B.get(1, 1) == 5);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 12 ===" << std::endl;
	{
		Mat33d A({ {
			{1,0,0},
			{0,1,0},
			{0,0,1}
        } });

		auto B = A.inv();
		assert(B.get(0, 0) == 1);
		assert(B.get(0, 1) == 0);
		assert(B.get(0, 2) == 0);
		assert(B.get(1, 0) == 0);
		assert(B.get(1, 1) == 1);
		assert(B.get(1, 2) == 0);
		assert(B.get(2, 0) == 0);
		assert(B.get(2, 1) == 0);
		assert(B.get(2, 2) == 1);
	}
	std::cout << "Done!" << std::endl;

	return 0;
}