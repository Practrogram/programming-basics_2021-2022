#include <iostream>

void CountSort(int x[], int n)
{
	int y[10];
	int c[10];
	int m = x[0];

	for (int i = 1; i < n; i++)
		if (x[i] > m)
			m = x[i];

	for (int i = 0; i <= m; i++)
		c[i] = 0;

	for (int i = 0; i < n; i++)
	{
		int k = x[i];
		c[k]++;
	}

	for (int i = 1; i <= m; i++)
		c[i] += c[i - 1];

	for (int i = n - 1; i >= 0; i--)
	{
		int k = x[i];
		y[c[k] - 1] = k;
		c[k]--;
	}

	for (int i = 0; i < n; i++)
		x[i] = y[i];
}

int main()
{
	int x[] = { 4, 2, 2, 8, 3, 3, 1 };
	int n = sizeof(x) / sizeof(x[0]);
	CountSort(x, n);
	for (int i = 0; i < n; i++)
		std::cout << x[i] << " ";
	std::cout << std::endl;

	int z;
	std::cin >> z;

	return 0;
}