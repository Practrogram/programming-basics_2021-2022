#include "matrix.hpp"
#include <iostream>

namespace an {
	Matrix::Matrix(int n, int m)
	{
		std::cout << "Constructor" << std::endl;
		a_n = n;
		a_m = m;
		a_mat = new double* [a_n];
		for (int i = 0; i < a_n; i++)
			a_mat[i] = new double[a_m];
	}

	Matrix::Matrix(const Matrix& mat)
	{
		a_n = mat.a_n;
		a_m = mat.a_m;

		a_mat = new double* [a_n];
		for (int i = 0; i < a_n; i++)
			a_mat[i] = new double[a_m];

		for (int i = 0; i < a_n; i++)
			for (int j = 0; j < a_m; j++)
				a_mat[i][j] = mat.a_mat[i][j];
	}

	Matrix& Matrix::operator=(const Matrix& mat)
	{
		a_n = mat.a_n;
		a_m = mat.a_m;

		for (int i = 0; i < a_n; i++)
			for (int j = 0; j < a_m; j++)
				a_mat[i][j] = mat.a_mat[i][j];

		return *this;
	}

	Matrix Matrix::operator+(const Matrix& mat)
	{
		Matrix tmp(2, 3);
		for (int i = 0; i < a_n; i++)
			for (int j = 0; j < a_m; j++)
				tmp.a_mat[i][j] = a_mat[i][j] + mat.a_mat[i][j];
		return tmp;
	}

	Matrix Matrix::operator-(const Matrix& mat)
	{
		Matrix tmp(2, 3);
		for (int i = 0; i < a_n; i++)
			for (int j = 0; j < a_m; j++)
				tmp.a_mat[i][j] = a_mat[i][j] - mat.a_mat[i][j];
		return tmp;
	}

	Matrix Matrix::operator*(const Matrix& mat)
	{
		Matrix tmp(a_n, mat.a_m);
		for (int i = 0; i < a_n; i++)
			for (int j = 0; j < mat.a_m; j++) {
				tmp.a_mat[i][j] = 0;
				for (int k = 0; k < a_m; k++)
					tmp.a_mat[i][j] += a_mat[i][k] * mat.a_mat[k][j];
			}
		return tmp;
	}

	int Matrix::determinant(const Matrix& mat) {
		int det = 0;
		if ((a_n == 2) && (a_m == 2)) {
			det = a_mat[0][0] * a_mat[1][1] - a_mat[1][0] * a_mat[0][1];
			return det;
		}
		else if ((a_n == 3) && (a_m == 3)) {
			det = a_mat[0][0] * (a_mat[1][1] * a_mat[2][2] - a_mat[1][2] * a_mat[2][1]) - a_mat[0][1] * (a_mat[1][0] * a_mat[2][2] - a_mat[1][2] * a_mat[2][0]) + a_mat[0][2] * (a_mat[1][0] * a_mat[2][1] - a_mat[1][1] * a_mat[2][0]);
			return det;
		}
		else {
			std::cout << "Operation not supported!" << std::endl;
			return -1;
		}
	}

	Matrix Matrix::inv(const Matrix& mat) {
		Matrix tmp(a_n, a_m);
		if ((a_n == 2 && a_m == 2) || (a_n == 3 && a_m == 3)) {
			int det = determinant(mat);
			if (det == 0)
				throw std::exception("Zero determinant!");
			if (a_n == 2) {
				tmp.a_mat[0][0] = a_mat[1][1] / det;
				tmp.a_mat[0][1] = -a_mat[0][1] / det;
				tmp.a_mat[1][0] = -a_mat[1][0] / det;
				tmp.a_mat[1][1] = a_mat[0][0] / det;
				return tmp;
			}
			if (a_n == 3) {
				tmp.a_mat[0][0] = (a_mat[1][1] * a_mat[2][2] - a_mat[2][1] * a_mat[1][2]) / det;
				tmp.a_mat[1][0] = (a_mat[1][0] * a_mat[2][2] - a_mat[2][0] * a_mat[1][2]) / det;
				tmp.a_mat[2][0] = (a_mat[1][0] * a_mat[2][1] - a_mat[2][0] * a_mat[1][1]) / det;
				tmp.a_mat[0][1] = (a_mat[0][1] * a_mat[2][2] - a_mat[2][1] * a_mat[0][2]) / det;
				tmp.a_mat[1][1] = (a_mat[0][0] * a_mat[2][2] - a_mat[2][0] * a_mat[0][2]) / det;
				tmp.a_mat[2][1] = (a_mat[0][0] * a_mat[2][1] - a_mat[2][0] * a_mat[0][1]) / det;
				tmp.a_mat[0][2] = (a_mat[0][1] * a_mat[1][2] - a_mat[1][1] * a_mat[0][2]) / det;
				tmp.a_mat[1][2] = (a_mat[0][0] * a_mat[1][2] - a_mat[1][0] * a_mat[0][2]) / det;
				tmp.a_mat[2][2] = (a_mat[0][0] * a_mat[1][1] - a_mat[1][0] * a_mat[0][1]) / det;
				return tmp;
			}
		}
		else {
			std::cout << "Operation not supported!" << std::endl;
		}
	}

	Matrix Matrix::transposed(const Matrix& mat) {
		Matrix tmp(a_n, a_m);
		for (int i = 0; i < a_n; i++)
			for (int j = 0; j < a_m; j++)
				tmp.a_mat[i][j] = mat.a_mat[j][i];
		return tmp;
	}

	Matrix::~Matrix()
	{
		std::cout << "Destructor" << std::endl;
		for (int i = 0; i < a_n; i++)
			delete[] a_mat[i];
		delete a_mat;
	}

	std::istream& operator>>(std::istream& in, Matrix& mat)
	{
		for (int i = 0; i < mat.a_n; i++)
			for (int j = 0; j < mat.a_m; j++)
				in >> mat.a_mat[i][j];
		return in;
	}

	std::ostream& operator<<(std::ostream& out, const Matrix& mat)
	{
		out << "Matrix " << mat.a_n << "x" << mat.a_m << std::endl;
		for (int i = 0; i < mat.a_n; i++) {
			for (int j = 0; j < mat.a_m; j++)
				out << mat.a_mat[i][j] << " ";
			out << std::endl;
		}
		return out;
	}
}
