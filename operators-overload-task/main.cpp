#include <iostream>
#include "matrix.hpp"

int main()
{
	an::Matrix A(3, 3);
	std::cin >> A;
	std::cout << A.transposed(A) << std::endl;
	std::cout << A.determinant(A) << std::endl;
	std::cout << A.inv(A) << std::endl;

	return 0;
}
