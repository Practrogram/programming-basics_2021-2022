#pragma once
#include <iostream>

namespace an
{
	class Matrix
	{
	public:
		Matrix(int n, int m);
		Matrix(const Matrix& mat);
		Matrix& operator=(const Matrix& mat);
		Matrix operator+(const Matrix& mat);
		Matrix operator*(const Matrix& mat);
		Matrix operator-(const Matrix& mat);
		int determinant(const Matrix& mat);
		Matrix inv(const Matrix& mat);
		Matrix transposed(const Matrix& mat);
		~Matrix();

		friend std::istream& operator>>(std::istream& os, Matrix& mat);
		friend std::ostream& operator<<(std::ostream& os, const Matrix& mat);

	private:
		int a_n, a_m;
		double** a_mat;
	};
}